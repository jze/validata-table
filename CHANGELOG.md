# CHANGELOG

## 0.11.1

- feat(ci): deployment to Pypi temporarily suspended, as long as a fork of 
  frictionless is used

## 0.11.0

- feat: remote file formats with urls without file extensions are infered from Content-Type header [MR34](https://gitlab.com/validata-table/validata-table/-/merge_requests/34)
- fix: invalid dates do not throw a python error [MR31](https://gitlab.com/validata-table/validata-table/-/merge_requests/31)
- refacto: use frictionless v5 [MR27](https://gitlab.com/validata-table/validata-table/-/merge_requests/27) (currently : fork, waiting for [bug](https://github.com/frictionlessdata/frictionless-py/pull/1615) [fixes](https://github.com/frictionlessdata/frictionless-py/pull/1641) to be merged)
- refacto: tend towards hexagonal architecture

## 0.10.3

Edit gitlab CI configuration: include build docker images and push to gitlab container registry 
step to build the two Docker images validata-table-ui and validata-table-api (respectively
used for Validata UI and Validata API) and host them on gitlab container registry
automatically at new tag of release

## 0.10.2

- Improve dockerization process to remove source code in docker images created
- Add linting tools in CI such as `black`, `flake8`, `isort` and fix errors associated
- Improve code coverage in tests
- Fix static typing errors identified with `pyright` tool

## 0.10.1

Edit gitlab CI: use `twine` tool to publish `validata-table` package in PyPI

## 0.10.0

Init `validata-table` package with three subpackages `validata_core`,
`validata_ui` and `validata_api`, as a consequence of merging in the same
gitlab mono-repository [Validata Table](https://gitlab.com/validata-table/validata-table)
the three gitlab repositories:
- [Validata core](https://gitlab.com/validata-table/validata-core)
- [Validata API](https://gitlab.com/validata-table/validata-api)
- [Validata UI](https://gitlab.com/validata-table/validata-ui)

The init number of version 0.10.0 is resulting from this 
[discussion](https://gitlab.com/validata-table/validata-table/-/issues/28#note_1599202925)
