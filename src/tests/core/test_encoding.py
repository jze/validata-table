import pytest

from validata_core import resource_service
from validata_core.domain.types import ValidataSourceError
from validata_core.infrastructure.resource_readers import _detect_encoding


@pytest.fixture
def irve_accent_valide_content():
    return open("tests/core/fixtures/irve_accent_valide.csv", "rb").read()


@pytest.fixture
def irve_driveco_content():
    return open("tests/core/fixtures/irve-driveco-201215.csv", "rb").read()


@pytest.fixture
def with_bom_content():
    return open("tests/core/fixtures/sample_with_bom.csv", "rb").read()


@pytest.fixture
def invalid_utf16_content():
    return open(
        "tests/core/fixtures/mini-menus-collectifs_invalide.utf-16.csv",
        "rb",
    ).read()


def test_detect_utf_8(irve_accent_valide_content):
    b_content = irve_accent_valide_content
    assert _detect_encoding(b_content) == "utf-8"


def test_read_utf_8(irve_accent_valide_content):
    header = resource_service.from_file_content(
        "foo.csv", irve_accent_valide_content
    ).header()
    assert "accessibilité" in header


def test_detect_iso8859_1(irve_driveco_content):
    b_content = irve_driveco_content
    assert _detect_encoding(b_content) == "iso8859-1"


def test_read_iso8859_1(irve_driveco_content):
    header = resource_service.from_file_content(
        "foo.csv", irve_driveco_content
    ).header()
    assert "accessibilité" in header, header


def test_ignore_bom(with_bom_content):
    header = resource_service.from_file_content(
        "with_bom.csv", with_bom_content
    ).header()
    assert header
    assert header[0] == "id", header


def test_invalid_utf16(invalid_utf16_content):
    with pytest.raises(ValidataSourceError):
        resource_service.from_file_content(
            "invalid_utf16.csv", invalid_utf16_content
        ).header()

    with pytest.raises(ValidataSourceError):
        resource_service.from_file_content(
            "invalid_utf16.csv", invalid_utf16_content
        ).rows()
