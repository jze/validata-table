import json
from pathlib import Path
from typing import Any, Dict

import pytest

import validata_core
from validata_core.infrastructure.resource_to_dict import FrictionlessToDictService


@pytest.fixture()
def expected_valid_formatted_report():
    with Path("tests/core/fixtures/expected_valid_formatted_report.json").open(
        "rt", encoding="utf-8"
    ) as fd:
        return json.load(fd)


@pytest.fixture()
def to_dict_service():
    return FrictionlessToDictService()


def recursivly_check_content(
    formatted_dict: Dict[str, Any], expected_formatted_dict: Dict[str, Any]
):
    """This function aims to check the content of the formatted dict refleting a validation report:
    The content of the formatted_dict should correspond the expected_formatted_dict.
    Check the values of all sub-items contained in each dictionnaries and
    sub-dictionnaries contained in the formatted_dict compared to the values contained in
    the expected_formatted_dict.
    Some specified key values are ignored in the checking for which only types'values are checked,
        - values corresponding at duration or date values associated to `date`, `second`, and `̀time` keys,
    """
    assert formatted_dict
    assert set(formatted_dict.keys()) == set(expected_formatted_dict.keys())

    for key, formatted_value in formatted_dict.items():
        expected_value = expected_formatted_dict[key]

        if isinstance(formatted_value, dict):
            assert isinstance(expected_value, dict)
            assert set(formatted_value.keys()) == set(expected_value.keys())
            recursivly_check_content(formatted_value, expected_value)

        elif isinstance(formatted_value, list) and formatted_value:
            assert isinstance(expected_value, list)

            for formatted_item, expected_item in zip(formatted_value, expected_value):
                if isinstance(formatted_item, dict):
                    assert set(formatted_item.keys()) == set(expected_item.keys())
                    recursivly_check_content(formatted_item, expected_item)

                else:
                    assert formatted_item == expected_item
        else:
            if key not in ["date", "time", "seconds"]:
                assert formatted_value == expected_value
            elif key == "date":
                assert isinstance(formatted_value, str)
            else:  # "time" or "seconds"
                assert isinstance(formatted_value, int) or isinstance(
                    formatted_value, float
                )


def test_format_valid_report(expected_valid_formatted_report, to_dict_service):
    """This test aims to check formatted report used for API returns
    GIVEN a tabular data resource and the associated schema
    WHEN I format the valid report of validation rended by validata_core
    EXPECT the well formatted report containing all properties which were in
    frictionless v4.38.0
    """

    source = [["A", "B", "C"], ["a", "b", "c"]]
    schema = {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {"name": "A", "type": "string"},
            {"name": "B", "type": "string"},
            {"name": "C", "type": "string"},
        ],
        "name": "tabular-schema",
        "title": "Tabular schema",
    }

    validata_valid_report = validata_core.validate(source, schema)
    assert validata_valid_report.valid
    validata_dict_report = to_dict_service.report_to_dict(validata_valid_report)
    recursivly_check_content(validata_dict_report, expected_valid_formatted_report)


@pytest.fixture()
def expected_invalid_formatted_report():
    with Path("tests/core/fixtures/expected_invalid_formatted_report.json").open(
        "rt", encoding="utf-8"
    ) as fd:
        return json.load(fd)


def test_format_invalid_report(expected_invalid_formatted_report, to_dict_service):
    """This test aims to check formatted report used for API returns
    GIVEN a tabular data resource and the associated schema
    WHEN I format the invalid report of validation rended by validata_core
    EXPECT the well formatted report containing all properties which were contained in
    frictionless v4.38.0
    """

    source = [["B", "C"], ["invalid_siret", "c"]]
    schema = {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {"name": "A", "type": "string", "constraints": {"required": True}},
            {"name": "B", "type": "string"},
            {"name": "C", "type": "string"},
        ],
        "name": "tabular-schema",
        "title": "Tabular schema",
        "custom_checks": [{"name": "french-siret-value", "params": {"column": "B"}}],
    }

    validata_invalid_report = validata_core.validate(source, schema)

    assert not validata_invalid_report.valid

    validata_dict_report = to_dict_service.report_to_dict(validata_invalid_report)
    recursivly_check_content(validata_dict_report, expected_invalid_formatted_report)


@pytest.fixture()
def expected_invalid_with_warnings_formatted_report():
    with Path(
        "tests/core/fixtures/expected_invalid_with_warnings_formatted_report.json"
    ).open("rt", encoding="utf-8") as fd:
        return json.load(fd)


def test_format_invalid_with_warnings_report(
    expected_invalid_with_warnings_formatted_report,
    to_dict_service,
):
    """This test aims to check formatted report used for API returns
    GIVEN a tabular data resource and the associated schema
    WHEN I format the invalid report of validation containing warnings rended by validata_core
    EXPECT the well formatted report containing all properties which were contained in
    frictionless v4.38.0
    """

    source = [["B", "C"], ["invalid_siret", "c"]]
    schema = {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {"name": "A", "type": "string"},
            {"name": "B", "type": "string"},
            {"name": "C", "type": "string"},
        ],
        "name": "tabular-schema",
        "title": "Tabular schema",
        "custom_checks": [{"name": "french-siret-value", "params": {"column": "B"}}],
    }

    validata_invalid_report = validata_core.validate(source, schema)

    assert not validata_invalid_report.valid
    validata_dict_report = to_dict_service.report_to_dict(validata_invalid_report)
    recursivly_check_content(
        validata_dict_report, expected_invalid_with_warnings_formatted_report
    )


@pytest.fixture()
def expected_invalid_with_schema_error_formatted_report():
    with Path(
        "tests/core/fixtures/expected_invalid_with_schema_error_formatted_report.json"
    ).open("rt", encoding="utf-8") as fd:
        return json.load(fd)


def test_format_invalid_with_schema_error_report(
    expected_invalid_with_schema_error_formatted_report,
    to_dict_service,
):
    """This test aims to check formatted report used for API returns
    GIVEN a tabular data resource and the associated schema
    WHEN I format the invalid report of validation containing schema error rended by validata_core
    EXPECT the well formatted report containing all properties which were contained in
    frictionless v4.38.0
    """

    source = [["A", "B", "C"], ["a", "b", "c"]]
    schema = {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {"name": "A", "type": "string"},
            {"name": "B", "type": "string"},
            {"name": "C", "type": "string"},
        ],
        "name": "UNVALID SCHEMA NAME",
        "title": "Tabular schema",
    }

    validata_invalid_report = validata_core.validate(source, schema)

    assert not validata_invalid_report.valid
    validata_dict_report = to_dict_service.report_to_dict(validata_invalid_report)

    recursivly_check_content(
        validata_dict_report, expected_invalid_with_schema_error_formatted_report
    )
