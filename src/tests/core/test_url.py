import pytest

from validata_core import resource_service
from validata_core.domain.types import ValidataSourceError


def test_url_resource_without_meaningful_ext():
    url = "https://demo.data.gouv.fr/fr/datasets/r/d4d78089-f218-40b6-a86a-d7cc4e624b0c"
    with pytest.raises(ValidataSourceError):
        resource_service.from_remote_file(url)
