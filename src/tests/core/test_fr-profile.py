from dataclasses import dataclass
from typing import Any, Dict, List

from validata_core import (
    ValidationFeatures,
    frictionless_table_schema_service,
    frictionless_validation_service,
    local_content_fetcher,
    remote_content_fetcher,
    resource_service,
)
from validata_core.domain.spi import CustomFormatsRepository
from validata_core.domain.types import CellValue, Validator


@dataclass
class SingleValidValueFormat:
    name: str
    valid_value: str
    description: str = ""

    def is_valid(self, value: str) -> bool:
        return value == self.valid_value


class SingleValidValueChecks(CustomFormatsRepository):
    """Test Custom Formats, where each format is associated to a single valid
    value"""

    def __init__(self, formats: List[SingleValidValueFormat]):
        self._available_formats = {f.name: f for f in formats}

    def ls(self) -> List[str]:
        return list(self._available_formats.keys())

    def get_validator(self, format) -> Validator:
        def validate(value: str, **kwargs):
            f = self._available_formats[format]
            return f.is_valid(value), ""

        return validate

    def get_description(self, format):
        return self._available_formats[format].description


class ForcedValidationCheck(CustomFormatsRepository):
    """Custom Formats used for tests, with a single accepted format "forced_validation".
    The output of the associated validator is given as an additional parameter
    "forced_value".
    """

    def ls(self) -> List[str]:
        return ["forced_validition"]

    def get_validator(self, format) -> Validator:
        def validate(value: str, **kwargs):
            forced_value = kwargs["forced_value"]
            if isinstance(forced_value, bool):
                return forced_value, ""
            else:
                return forced_value == "true", ""

        return validate

    def get_description(self, format):
        return ""


def make_validation_service(custom_formats):
    # Inject a fake custom formats dependency for tests
    return ValidationFeatures(
        frictionless_validation_service,
        frictionless_table_schema_service,
        resource_service,
        remote_content_fetcher,
        local_content_fetcher,
        custom_formats,
    )


def _make_schema_with_frformat(format, extra_field=False):
    fields = [{"name": "field1", "frFormat": format}]

    if extra_field:
        fields.append({"name": "field2"})

    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": fields,
    }


def test_validation_with_fr_format():
    test_validation_service = make_validation_service(
        SingleValidValueChecks(
            [
                SingleValidValueFormat(name="siret", valid_value="valid_siret"),
                SingleValidValueFormat(name="siren", valid_value="valid_siren"),
            ]
        )
    )
    test_cases = [
        {
            "name": "Valid value, string frformat",
            "frformat": "siret",
            "value": "valid_siret",
            "expect_valid": True,
        },
        {
            "name": "Valid value 2, string frformat",
            "frformat": "siren",
            "value": "valid_siren",
            "expect_valid": True,
        },
        {
            "name": "Invalid value, string frformat",
            "frformat": "siret",
            "value": "invalid_siret",
            "expect_valid": False,
        },
        {
            "name": "Invalid value 2, string frformat",
            "frformat": "siren",
            "value": "invalid_siren",
            "expect_valid": False,
        },
        {
            "name": "Valid value for second field, string frformat",
            "frformat": "siret",
            "extra_field": True,
            "value": "valid_siret",
            "expect_valid": True,
        },
        {
            "name": "Valid value, object frformat",
            "frformat": {"name": "siret"},
            "value": "valid_siret",
            "expect_valid": True,
        },
        {
            "name": "Invalid value, object frformat",
            "frformat": {"name": "siren"},
            "value": "invalid_siren",
            "expect_valid": False,
        },
    ]

    for tc in test_cases:
        has_extra_field = "extra_field" in tc and tc["extra_field"]

        data = [{"field1": tc["value"]}]
        if has_extra_field:
            data[0]["field2"] = "some_value"

        schema = _make_schema_with_frformat(tc["frformat"], has_extra_field)

        validata_source = resource_service.from_inline_data(data)
        report = test_validation_service.validate_resource(validata_source, schema)
        assert report.valid == tc["expect_valid"], tc["name"]


def test_format_with_parameters():
    test_validation_service = make_validation_service(ForcedValidationCheck())
    test_cases = [
        {
            "test_name": (
                "Extra boolean parameter passed to object frformat, "
                "validation is forced to True"
            ),
            "frformat": {
                "name": "forced_validition",
                "forced_value": True,
            },
            "expect_valid": True,
        },
        {
            "test_name": (
                "Extra boolean parameter passed to object frformat, "
                "validation is forced to False"
            ),
            "frformat": {
                "name": "forced_validition",
                "forced_value": False,
            },
            "expect_valid": False,
        },
        {
            "test_name": (
                "Extra string parameter passed to object frformat, "
                "validation is forced to True"
            ),
            "frformat": {
                "name": "forced_validition",
                "forced_value": "true",
            },
            "expect_valid": True,
        },
        {
            "test_name": (
                "Extra boolean parameter passed to object frformat, "
                "validation is forced to False"
            ),
            "frformat": {
                "name": "forced_validition",
                "forced_value": "false",
            },
            "expect_valid": False,
        },
    ]

    for tc in test_cases:
        schema = _make_schema_with_frformat(tc["frformat"])

        data: Any = [{"field1": "whatever"}]
        validata_source = resource_service.from_inline_data(data)

        report = test_validation_service.validate_resource(validata_source, schema)
        assert report.valid == tc["expect_valid"], tc["test_name"]


def test_invalid_fr_format():
    test_cases = [
        {
            "frformat": "invalid",
            "valid_formats": ["siren", "siret"],
        },
        {
            "frformat": "siret",
            "valid_formats": [],
        },
        {
            "frformat": "siret",
            "valid_formats": ["siren"],
        },
    ]

    ### Test helpers to test both str and object formats for frformat
    def _convert_frformat_to_object_form(test_case):
        """Assumes frformat is in string form"""
        test_case["frformat"] = {"name": test_case["frformat"]}
        return test_case

    for tc in test_cases:
        if isinstance(tc["frformat"], str):
            test_cases.append(_convert_frformat_to_object_form(tc))

    for tc in test_cases:
        schema = _make_schema_with_frformat("FIELD", tc["frformat"])
        validation_service = make_validation_service(
            SingleValidValueChecks(
                [SingleValidValueFormat(f, "") for f in tc["valid_formats"]]
            )
        )
        inline_data: List[Dict[str, CellValue]] = [{"SIRET": "abc"}]
        validata_source = resource_service.from_inline_data(inline_data)
        report = validation_service.validate_resource(validata_source, schema)

        assert not report.valid
        errs = report.tasks[0].errors

        assert len(errs) == 1
        assert errs[0].type == "check-error"


def test_single_invalid_format_does_not_prevent_subsequent_checks():
    test_validation_service = make_validation_service(
        SingleValidValueChecks(
            [SingleValidValueFormat(name="siret", valid_value="valid_siret")]
        )
    )
    tc = {
        "field1_frformat": "invalid",
        "field2_frformat": "siret",
        "field2_value": "invalid_siret",
        "expect_valid": False,
    }

    schema = _make_schema_with_frformat(tc["field1_frformat"], True)
    schema["fields"][1]["frFormat"] = tc["field2_frformat"]

    data = [{"field1": "some value", "field2": tc["field2_value"]}]

    validata_source = resource_service.from_inline_data(data)
    report = test_validation_service.validate_resource(validata_source, schema)

    assert not report.valid
    errs = report.tasks[0].errors

    assert len(errs) == 2
    types = [err.type for err in errs]
    assert "check-error" in types
    assert "siret-fr-format" in types


def test_explicit_error_message():
    test_validation_service = make_validation_service(
        SingleValidValueChecks(
            [
                SingleValidValueFormat(
                    name="blanquette",
                    valid_value="Comment est votre blanquette ?",
                    description="La blanquette est bonne",
                ),
                SingleValidValueFormat(
                    name="vie-univers",
                    valid_value="42",
                    description="Réponse sur la vie, l'univers et le reste",
                ),
            ]
        )
    )
    test_cases = [
        {
            "name": "Invalid check 1 type",
            "frformat": "blanquette",
            "expect_exact_string": "blanquette-fr-format",
            "in_property": "type",
        },
        {
            "name": "Invalid check 2 type",
            "frformat": "vie-univers",
            "expect_exact_string": "vie-univers-fr-format",
            "in_property": "type",
        },
        {
            "name": "Invalid check 1 title",
            "frformat": "blanquette",
            "expect_exact_string": "Blanquette invalide",
            "in_property": "title",
        },
        {
            "name": "Invalid check 2 title",
            "frformat": "vie-univers",
            "expect_exact_string": "Vie Univers invalide",
            "in_property": "title",
        },
        {
            "name": "Invalid check 1 description",
            "frformat": "blanquette",
            "expect_substring": "La blanquette est bonne",
            "in_property": "description",
        },
        {
            "name": "Invalid check 2 description",
            "frformat": "vie-univers",
            "expect_substring": "Réponse sur la vie, l'univers et le reste",
            "in_property": "description",
        },
    ]

    for tc in test_cases:
        data = [{"field1": "invalid"}]
        schema = _make_schema_with_frformat(tc["frformat"])
        validata_source = resource_service.from_inline_data(data)  # type: ignore
        report = test_validation_service.validate_resource(validata_source, schema)

        assert not report.valid
        errs = report.tasks[0].errors

        assert len(errs) == 1

        property_value = ""
        if tc["in_property"] == "title":
            property_value = errs[0].title
        elif tc["in_property"] == "description":
            property_value = errs[0].description
        if tc["in_property"] == "type":
            property_value = errs[0].type

        if "expect_substring" in tc:
            assert tc["expect_substring"] in property_value

        if "expect_exact_string" in tc:
            assert tc["expect_exact_string"] == property_value
