import pytest

from validata_core import validate


def test_incorrect_date_message():
    schema_with_date = {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [{"name": "date", "type": "date", "format": "%Y-%m-%d"}],
    }

    date = "37/03/2024"
    source = [["date"], [date]]

    try:
        validate(source, schema_with_date)
    except Exception:
        pytest.fail(f"Validation fails with date { date }")
