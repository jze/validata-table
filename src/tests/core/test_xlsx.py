import json
from pathlib import Path

import pytest

from validata_core import resource_service as resource
from validata_core import validate


def validate_xlsx_bytes(bytes_source_content, schema):
    validata_source = resource.from_file_content("foo.xlsx", bytes_source_content)
    return validate(validata_source.to_inline_data(), schema)


@pytest.fixture()
def dae_schema():
    with Path("tests/core/fixtures/dae_schema.json").open("rt", encoding="utf-8") as fd:
        return json.load(fd)


def test_validate_dae_xlsx_file(dae_schema):
    with Path("tests/core/fixtures/dae_test.xlsx").open("rb") as fd:
        bytes_content = fd.read()

    validata_source = resource.from_file_content("dae_test.xlsx", bytes_content)
    report = validate(validata_source.to_inline_data(), dae_schema)
    assert report.valid


def test_validate_dae_xlsx_url(dae_schema):
    validata_source = resource.from_remote_file(
        "https://gitlab.com/validata-table/validata-ui/uploads/dc50b16c803e2e2d61a645019cd75b3c/test1.xlsx"
    )
    report = validate(validata_source.to_inline_data(), dae_schema)
    assert report.valid
