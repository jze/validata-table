from typing import List

from validata_core.domain.types import Error, Report


def assert_valid_report(report: Report) -> None:
    assert report.valid
    assert not get_report_errors(report)
    assert len(report.tasks) == 1


def get_report_errors(report: Report) -> List[Error]:
    assert report.tasks[0]
    return report.tasks[0].errors


def assert_no_error(report: Report) -> None:
    assert report.stats["errors"] == 0
    assert not get_report_errors(report)


def assert_single_error(report: Report) -> None:
    assert report.stats["errors"] == 1
    assert report.stats["tasks"] == 1
    assert len(get_report_errors(report)) == 1


def get_report_warnings(report: Report) -> List[str]:
    assert report.tasks[0]
    return report.tasks[0].warnings


def assert_no_warning(report: Report) -> None:
    assert report.stats["warnings"] == 0
    assert not get_report_warnings(report)


def assert_single_warning(report: Report) -> None:
    assert report.stats["warnings"] == 1
    assert len(get_report_warnings(report)) == 1


def assert_warning_message(warning: str, message: str) -> None:
    assert warning == message
