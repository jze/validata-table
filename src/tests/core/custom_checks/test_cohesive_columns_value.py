import pytest

from tests.core import utils
from validata_core import validate


def _schema_cohesive_columns():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {"name": "id", "title": "Identifiant", "type": "number"},
            {"name": "col1", "title": "Colonne 1", "type": "string"},
            {"name": "col2", "title": "Colonne 2", "type": "string"},
        ],
        "custom_checks": [
            {
                "name": "cohesive-columns-value",
                "params": {"column": "col1", "othercolumns": ["col2"]},
            }
        ],
    }


@pytest.fixture
def schema_cohesive_columns():
    return _schema_cohesive_columns()


def test_cohesive_columns_values_valid(schema_cohesive_columns):
    sources = [
        [["id", "col1", "col2"], [1, None, None]],
        [["id", "col1", "col2"], [1, "foo", "bar"]],
        [["id"], [1]],
    ]

    for source in sources:
        report = validate(source, schema_cohesive_columns)
        assert report.valid


def test_cohesive_columns_values_invalid(schema_cohesive_columns):
    sources = [
        [["id", "col1", "col2"], [1, "foo", None]],
        [["id", "col1", "col2"], [1, None, "bar"]],
    ]

    for source in sources:
        report = validate(source, schema_cohesive_columns)
        utils.assert_single_error(report)
        error = utils.get_report_errors(report)[0]
        assert error.type == "cohesive-columns-value"


@pytest.fixture
def schema_cohesive_columns_on_required_field():
    schema_cohesive_on_required_field = _schema_cohesive_columns()
    schema_cohesive_on_required_field["fields"][1]["constraints"] = {"required": True}
    return schema_cohesive_on_required_field


def test_cohesive_columns_values_required(schema_cohesive_columns_on_required_field):
    source = [["id", "col1", "col2"], [1, None, "bar"]]
    report = validate(source, schema_cohesive_columns_on_required_field)
    assert not report.valid
    report_errors = utils.get_report_errors(report)
    assert len(report_errors) == 2
    assert report_errors[0].type == "constraint-error"
    assert report_errors[0].title == "Cellule vide"
    assert report_errors[1].type == "cohesive-columns-value"
    assert report_errors[1].title == "Cohérence entre colonnes"


def test_cohesive_columns_values_missing_columns(
    schema_cohesive_columns_on_required_field,
):
    # GIVEN One on the column (not required) relative to the custom check (col2) does not exist
    # EXPECT validation error with one error :
    # - check-error related to the fact that one column included in the custom check is missing ("col2")
    source = [["id", "col1"], [1, "foo"]]
    report = validate(source, schema_cohesive_columns_on_required_field)
    assert not report.valid
    utils.assert_single_error(report)
    error = utils.get_report_errors(report)[0]

    # Ensure that the error is a check-error related to the fact that one column included in the custom check is missing ("col1")
    assert error.type == "check-error"
    expected_message = (
        "'cohesive-columns-value': La colonne à comparer 'col2' est manquante"
    )
    assert expected_message in error.message


def test_cohesive_columns_values_required_missing_columns(
    schema_cohesive_columns_on_required_field,
):
    # GIVEN One ("col1") of the custom check columns specified as required does not exist
    # EXPECT validation error with two errors :
    # - one missing label error
    # - one check-error related to the fact that one column included in the custom check is missing ("col1")
    source = [["id", "col2"], [1, "bar"]]
    report = validate(source, schema_cohesive_columns_on_required_field)
    assert not report.valid
    report_errors = utils.get_report_errors(report)
    assert len(report_errors) == 2
    # Ensure that one error is a missing-label-error related to the missing required header column ("col1")
    assert report_errors[0].type == "missing-label"
    assert "Colonne obligatoire manquante" in report_errors[0].title
    # Ensure that one error is a check-error related to the fact that one column included in the custom check is missing ("col1")
    assert report_errors[1].type == "check-error"
    expected_message = "'cohesive-columns-value': La colonne 'col1' est manquante"
    assert expected_message in report_errors[1].message
