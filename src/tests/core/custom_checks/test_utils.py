from typing import Any, List, Optional, Type

import attrs
from frictionless import Detector, Row, errors
from typing_extensions import Self

from tests.core import utils
from validata_core import validate
from validata_core.custom_checks import available_checks
from validata_core.custom_checks.utils import (
    CustomCheckMultipleColumns,
    CustomCheckSingleColumn,
)


def _schema_custom_check_test():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [{"name": "A", "type": "integer"}, {"name": "B", "type": "integer"}],
        "custom_checks": [
            {"name": "custom-check-single-column-test", "params": {"column": "A"}}
        ],
    }


@attrs.define(kw_only=True, repr=False)
class MockCustomCheckSingleColumn(CustomCheckSingleColumn):
    """CustomCheckTest expects only `1` values"""

    Errors = [errors.ConstraintError]

    type = "custom-check-single-column-test"

    def _validate_start(self):
        yield from []

    def _validate_row(self, cell_value: Any, row: Row):
        if cell_value != 1:
            yield errors.ConstraintError.from_row(
                row,
                note="custom_check_single_column_test_error",
                field_name=self.column,
            )

    @classmethod
    def metadata_select_class(cls, type: Optional[str]) -> Type[Self]:
        return cls

    metadata_profile = {  # type: ignore
        "type": "object",
        "required": ["column"],
        "properties": {"column": {"type": "string"}},
    }


def test_custom_check_single_column():
    # Add mock custom check to the available custom checks list
    available_checks[MockCustomCheckSingleColumn.type] = MockCustomCheckSingleColumn

    schema = _schema_custom_check_test()

    # Test case: None value in column "A" -> ignore custom-check-single-column-test applied -> valid report
    source_valid = [["A", "B"], [None, 2]]
    report = validate(
        source=source_valid,
        schema_descriptor=schema,
    )
    assert report.valid

    # Test case: Correct value in column "A" -> do not ignore custom-check-single-column-test applied -> valid report
    source_valid = [["A", "B"], [1, 3]]
    report = validate(
        source=source_valid,
        schema_descriptor=schema,
    )
    assert report.valid

    # Test case: Incorrect value in column "A" ->  do not ignore custom-check-single-column-test applied -> invalid
    # report
    source_invalid = [["A", "B"], [2, 3]]
    report = validate(
        source=source_invalid,
        schema_descriptor=schema,
    )
    utils.assert_single_error(report)
    error = utils.get_report_errors(report)[0]
    assert error.type == "constraint-error"
    assert error.note == "custom_check_single_column_test_error"

    # Delete mock custom check from the available custom checks list
    del available_checks[MockCustomCheckSingleColumn.type]
    assert MockCustomCheckSingleColumn.type not in available_checks.keys()
    assert MockCustomCheckSingleColumn not in available_checks.values()


@attrs.define(kw_only=True, repr=False)
class MockCustomCheckMultipleColumn(CustomCheckMultipleColumns):
    """CustomCheckTest expects only `2` as sum of all values columns"""

    type = "custom-check-multiple-columns-test"

    def _validate_start(self, all_columns: List[str]):
        for col in all_columns:
            if col not in self.resource.schema.field_names:
                note = f"La colonne {col} n'est pas trouvée."
                yield errors.CheckError(note=f"{self.type}: {note}")

    def _validate_row(self, row: Row):
        cell_values = [row[col] for col in self.get_all_columns()]
        if sum(cell_values) != 2:
            yield errors.ConstraintError.from_row(
                row, note="custom_check_multiple_columns_test_error", field_name="A"
            )

    @classmethod
    def metadata_select_class(cls, type: Optional[str]) -> Type[Self]:
        return cls

    metadata_profile = {  # type: ignore
        "type": "object",
        "required": ["column"],
        "properties": {
            "column": {"type": "string"},
            "column2": {"type": "string"},
            "columns": {"type": "array"},
            "othercolumns": {"type": "array"},
        },
    }


def test_custom_check_multiple_columns_validate_row1():
    # Add mock custom check to the available custom checks list
    available_checks[MockCustomCheckMultipleColumn.type] = MockCustomCheckMultipleColumn

    # The custom check defined in the schema includes two columns : column and column2
    schema = _schema_custom_check_test()
    schema["custom_checks"][0]["name"] = "custom-check-multiple-columns-test"
    schema["custom_checks"][0]["params"]["column2"] = "B"

    # Test case: None value on column "A" or column "B" -> ignore custom-check-multiple-columns-test -> report valid
    sources = [[["A", "B"], [None, 3]], [["A", "B"], [3, None]]]
    for source in sources:
        report = validate(
            source,
            schema_descriptor=schema,
        )
        assert report.valid

    # Test case: Incorrect values on column "A" and "B" -> custom-check-multiple-columns-test is applied -> invalid report
    sources = [[["A", "B"], [3, 1]], [["A", "B"], [1, 3]]]
    for source in sources:
        report = validate(
            source,
            schema_descriptor=schema,
        )
        utils.assert_single_error(report)
        error = utils.get_report_errors(report)[0]
        assert error.type == "constraint-error"
        assert error.note == "custom_check_multiple_columns_test_error"

    # Test case: Correct values on column "A" and "B" -> custom-check-multiple-columns-test is applied -> valid report
    source = [["A", "B"], [1, 1]]
    report = validate(
        source,
        schema_descriptor=schema,
    )
    assert report.valid

    # Delete mock custom check from the available custom checks list
    del available_checks[MockCustomCheckMultipleColumn.type]
    assert MockCustomCheckMultipleColumn.type not in available_checks.keys()
    assert MockCustomCheckMultipleColumn not in available_checks.values()


def test_custom_check_multiple_columns_validate_row2():
    # Add mock custom check to the available custom checks list
    available_checks[MockCustomCheckMultipleColumn.type] = MockCustomCheckMultipleColumn

    # The custom check defined in the schema includes more than two columns : column and other_columns
    schema = _schema_custom_check_test()
    schema["fields"].append({"name": "C", "type": "integer"})
    schema["custom_checks"][0]["name"] = "custom-check-multiple-columns-test"
    schema["custom_checks"][0]["params"]["othercolumns"] = ["B", "C"]

    # Test case: None value on column "A" or column "B" or column "C -> ignore custom-check-multiple-columns-test
    # -> report valid
    sources = [
        [["A", "B", "C"], [3, 1, None]],
        [["A", "B", "C"], [None, 1, None]],
        [["A", "B", "C"], [None, None, 3]],
    ]
    for source in sources:
        report = validate(
            source,
            schema_descriptor=schema,
        )
        assert report.valid

    # Test case: Incorrect values on columns "A", "B" and "C" -> custom-check-multiple-columns-test is applied
    # -> invalid report
    source = [["A", "B", "C"], [3, 1, 1]]
    report = validate(
        source,
        schema_descriptor=schema,
    )
    utils.assert_single_error(report)
    error = utils.get_report_errors(report)[0]
    assert error.type == "constraint-error"
    assert error.note == "custom_check_multiple_columns_test_error"

    # Test case: Correct values on columns "A" and "B" and "C" -> custom-check-multiple-columns-test is applied ->
    # valid report
    source = [["A", "B", "C"], [1, 1, 0]]
    report = validate(
        source,
        schema_descriptor=schema,
    )
    assert report.valid

    # Delete mock custom check from the available custom checks list
    del available_checks[MockCustomCheckMultipleColumn.type]
    assert MockCustomCheckMultipleColumn.type not in available_checks.keys()
    assert MockCustomCheckMultipleColumn not in available_checks.values()


def test_custom_check_multiple_columns_validate_start1():
    # Add mock custom check to the available custom checks list
    available_checks[MockCustomCheckMultipleColumn.type] = MockCustomCheckMultipleColumn

    schema = _schema_custom_check_test()
    schema["custom_checks"][0]["name"] = "custom-check-multiple-columns-test"
    schema["custom_checks"][0]["params"]["column2"] = "B"

    # Test case: wrong name on column "A" related to custom check
    source = [["aA", "B"], [3, 1]]
    report = validate(
        source,
        schema_descriptor=schema,
        detector=Detector(schema_sync=True),
    )
    utils.assert_single_error(report)
    error = utils.get_report_errors(report)[0]
    assert error.type == "check-error"
    assert "La colonne A n'est pas trouvée." in error.note

    # Test case: wrong name on column "B" related to custom check
    source = [["A", "bB"], [3, 1]]
    report = validate(
        source,
        schema_descriptor=schema,
        detector=Detector(schema_sync=True),
    )
    utils.assert_single_error(report)
    error = utils.get_report_errors(report)[0]
    assert error.type == "check-error"
    assert "La colonne B n'est pas trouvée." in error.note

    # Delete mock custom check from the available custom checks list
    del available_checks[MockCustomCheckMultipleColumn.type]
    assert MockCustomCheckMultipleColumn.type not in available_checks.keys()
    assert MockCustomCheckMultipleColumn not in available_checks.values()
